<!DOCTYPE html>
<html>
<head>
	<title>Form</title>
</head>
<body>
	<h1>Buat Account Baru!</h1>
	<h4>Sign Up Form</h4>
	<div>
		<form action ="/welcomee" method="POST">
			@csrf

			<label for="first_name">First name: </label><br><br>
			<input type="text" name="nama" id="first_name"><br><br>
			<label for="last_name">Last name: </label><br><br>
			<input type="text" name="nama" id="last_name"><br><br>
			<label for="">Gender:</label><br><br>
			<input type="radio" name="Gender" value="male">Male <br>
			<input type="radio" name="Gender" value="female">Female<br>
			<input type="radio" name="Gender" value="other">Other<br><br>
			<label for="">Nationality:</label><br><br>
			<select>
				<option value="I">Indonesian</option>
				<option value="S">Singapore</option>
				<option value="M">Malaysian</option>

			</select><br><br>
			<label for="">Language Spoken:</label><br><br>
			<input type="checkbox" name="Language" value="B.Indo">Bahasa Indonesia<br>
			<input type="checkbox" name="Language" value="english">English<br>
			<input type="checkbox" name="Language" value="other">Other<br><br>

			<label for="Bio">Bio:</label><br><br>
			<textarea id="Bio" cols="30" rows="10"></textarea><br>
			
			<button type="submit" ><a href="/welcomee">Sign Up</button>
			

		</form>

	</div>
</body>
</html>